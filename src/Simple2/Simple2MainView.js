import React, { Component } from 'react';
import NormalFuncComponent from './NormalFuncComponent';
import FuncFuncComponent from './FuncFuncComponent';

// Different ways to render content in a component

export default class Simple2MainView extends Component {
    render() {
        return (
            <div>
                <h2>Simple2MainView</h2>
                <p>Different ways to create components and their render methods.</p>

                <NormalFuncComponent name="Donald Duck" />
                <FuncFuncComponent name="Daisy" />
            </div>
        );
    }
}
