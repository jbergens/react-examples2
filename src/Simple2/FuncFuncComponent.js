import React from 'react';
import PropTypes from 'prop-types';
import myStyles from './helloBoxStyle';

function InnerHello(name) {
    return (<h3>Hi {name}!</h3>);
}

const InnerMessage = () => (
    <p>This is a stateless component (FuncFuncComponent).</p>
);

function FuncFuncComponent(props) {
    return (
        <div style={myStyles}>
            { InnerHello(props.name) }
            { InnerMessage() }
        </div>
    );
}

FuncFuncComponent.propTypes = {
    name: PropTypes.string.isRequired,
};

export default FuncFuncComponent;
