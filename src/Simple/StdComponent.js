import React, { Component } from 'react';

export default class StdComponent extends Component {
    render() {
        return (<p>Hello {this.props.name}! </p>);
    }
}

// There is no props validation here in order to keep the code short.
// It would normally be a good idea to use props validation.
