import React from 'react';

function FuncComponentA(props) {
    return (
        <div className="App-box">
            <p>Hi  {props.name}! </p>
            <p>This is a stateless functional component. </p>
        </div>
    );
}

export default FuncComponentA;
