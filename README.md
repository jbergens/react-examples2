#Simple react examples v2

This demo contains some simple react code demos meant to be used during presentations or training courses. 
It uses the create-react-app tool to configure some tools like WebPack.

It can be started by running the following commands:

```
> yarn install
> yarn started
```
